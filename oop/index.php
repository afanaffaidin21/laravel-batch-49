<?php
    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');

    $sheep = new Animal("Shaun");
    echo "Name: " . $sheep->get_name() . "\n";
    echo "<br>";
    echo "legs: " . $sheep->get_legs() . "\n";
    echo "<br>";
    echo "cold blooded: " . $sheep->get_cold_blooded() . "\n";
    echo "<br><br>";

    $kodok = new Frog("Buduk");
    echo "Name: " . $kodok->get_name() . "\n";
    echo "<br>";
    echo "legs: " . $kodok->get_legs() . "\n";
    echo "<br>";
    echo "cold blooded: " . $kodok->get_cold_blooded() . "\n";
    echo "<br>";
    $kodok->jump(); // "hop hop"
    echo "<br> <br>";

    $sungokong = new Ape("Kera Sakti");
    echo "Name: " . $sungokong->get_name() . "\n";
    echo "<br>";
    echo "legs: " . $sungokong->get_legs() . "\n";
    echo "<br>";
    echo "cold blooded: " . $sungokong->get_cold_blooded() . "\n";
    echo "<br>";
    $sungokong->yell(); // "Auooo"
    echo "<br>";
?>