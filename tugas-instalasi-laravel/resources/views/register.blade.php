@extends('master')
@section('title')
Buat Account Baru!
@endsection
@section('content')
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
      @csrf
      <label>First name:</label><br /><br />
      <input type="text" name="nama_depan"/><br /><br />

      <label>Last name:</label><br /><br />
      <input type="text" name="nama_belakang"/><br /><br />

      <label>Gender:</label><br /><br />
      <input type="radio" name="the_gender" />Male <br />
      <input type="radio" name="the_gender" />Female <br />
      <input type="radio" name="the_gender" />Other <br /><br />

      <label>Nationality</label><br /><br />
      <select name="nationality">
        <option value="indonesian">Indonesian</option>
        <option value="malaysian">Malaysian</option>
        <option value="autralian">Autralian</option>
      </select>
      <br /><br />

      <label>Language Spoken</label><br /><br />
      <input type="checkbox" name="bahasa" />Bahasa Indonesia<br />
      <input type="checkbox" name="bahasa" />English<br />
      <input type="checkbox" name="bahasa" />Other<br /><br />

      <label>Bio:</label><br /><br />
      <textarea cols="30" rows="10"></textarea><br /><br />
      <input type="submit" value="Submit" />
    </form>
@endsection
