@extends('master')
@section('title')
Halaman Edit Cast
@endsection
@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
  <div class="form-group">
    <label>Nama</label>
    <input type="text" value="{{$cast->nama}}" name="nama" class="@error('nama') is-invalid @enderror form-control" placeholder="Masukan Nama">
  </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <div class="form-group">
    <label>Umur</label>
    <input type="text" value="{{$cast->umur}}" name="umur" class="form-control" placeholder="Masukan Umur">
  </div>
  <div class="form-group">
    <label>Bio</label>
    <textarea class="form-control" name="bio" cols="30" rows="10" placeholder="Masukan Bio">{{$cast->bio}}</textarea>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection