<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [HomeController::class,'home']);
Route::get('/register', [AuthController::class,'register']);
Route::post('/welcome', [AuthController::class,'welcome']);

Route::get('/table',function(){
    return view('table');
});

Route::get('/data-table',function(){
    return view('datatable');
});
// Route::get('/master',function(){
//     return view('master');
// });

// CRUD

// Create Data
// membuat route yang mengarah ke form tambah data
Route::get('/cast/create', [CastController::class, 'create']);
// membuat route untuk tambah data ke database
Route::post('/cast', [CastController::class, 'store']);

// Read data
// membuat route untuk menampilkan semua cast
Route::get('/cast', [CastController::class, 'index']);
//Route untuk detail cast berdasarkan id
Route::get('/cast/{id}', [CastController::class, 'show']);

//Update Data
//Route untuk mengarah ke form edit bdata dengan params id
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
//Route update data di database berdasarkan id
Route::put('/cast/{id}', [CastController::class, 'update']);

//Delate Data
Route::delete('/cast/{id}', [CastController::class, 'destroy']);

